import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './pages/index/index.component';
import { DispenserComponent } from './pages/dispenser/dispenser.component';
import { OfferComponent } from './pages/offer/offer.component';

@NgModule({
    declarations: [AppComponent, IndexComponent, DispenserComponent, OfferComponent],
    imports: [BrowserModule, AppRoutingModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
