import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispenserComponent } from './pages/dispenser/dispenser.component';
import { IndexComponent } from './pages/index/index.component';
import { OfferComponent } from './pages/offer/offer.component';

const routes: Routes = [
    { path: 'login', component: IndexComponent },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'dispenser', component: DispenserComponent },
    { path: 'offer', component: OfferComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
